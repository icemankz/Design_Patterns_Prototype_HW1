﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DP_HW1_Altynbekov
{
    public partial class FormPrototypeSample : Form
    {
        public FormPrototypeSample()
        {
            InitializeComponent();
        }

        private void buttonBrowseImage_Click(object sender, EventArgs e)
        {
            BrowseSourceImage();
        }

        private void BrowseSourceImage()
        {
            using (var ofd = new OpenFileDialog())
            {
                ofd.Filter = "Jpg file|*.jpg";
                ofd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                ofd.RestoreDirectory = true;

                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    var fileName = ofd.FileName;

                    pictureBoxSource.Image = Image.FromFile(fileName);
                    sourceImage = pictureBoxSource.Image;
                }
                else
                {
                    toolStripStatusLabel1.Text = "No file selected";
                }
            }
        }

        private void buttonClone_Click(object sender, EventArgs e)
        {
            //pictureBoxClone.Image = pictureBoxSource.Image;  
            Prototype prototype = new ConcretePrototype1(sourceImage);
            Prototype clone = prototype.Clone();
            pictureBoxClone.Image = clone.Image;
          
        }

        abstract class Prototype
        {
            public Image Image { get; private set; }
            public Prototype(Image image)
            {
                this.Image = image;
            }
            public abstract Prototype Clone();
        }

        class ConcretePrototype1 : Prototype
        {
            public ConcretePrototype1(Image image)
                : base(image)
            { }
            public override Prototype Clone()
            {
                return new ConcretePrototype1(Image);
            }
        }

        public static Image sourceImage { get; set; }
    }
}
